/**
   This file is part of FERRET, an add-on module for MOOSE

   FERRET is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a co_antiferrodis_A_y[_qp] of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   For help with FERRET please contact J. Mangeri <john.mangeri@uconn.edu>
   and be sure to track new changes at bitbucket.org/mesoscience/ferret

**/

#include "RotostrictiveCouplingEnergyDensity.h"

registerMooseObject("FerretApp", RotostrictiveCouplingEnergyDensity);

template<>
InputParameters validParams<RotostrictiveCouplingEnergyDensity>()
{
  InputParameters params = validParams<AuxKernel>();
  params.addRequiredCoupledVar("disp_x", "The x component of the displacement");
  params.addRequiredCoupledVar("disp_y", "The y component of the displacement");
  params.addCoupledVar("disp_z", 0.0, "The z component of the displacement");
  params.addRequiredCoupledVar("antiferrodis_A_x", "The x component of the antiferrodistortive tilt vector");
  params.addRequiredCoupledVar("antiferrodis_A_y", "The y component of the antiferrodistortive tilt vector");
  params.addCoupledVar("antiferrodis_A_z", 0.0, "The z component of the antiferrodistortive tilt vector");
  params.addRequiredParam<Real>("r11", "The coupling constants");
  params.addRequiredParam<Real>("r12", "The coupling constants");
  params.addRequiredParam<Real>("r44", "The coupling constants");
  params.addParam<Real>("len_scale", 1.0, "the len_scale of the unit");
  return params;
}

RotostrictiveCouplingEnergyDensity::RotostrictiveCouplingEnergyDensity(const InputParameters & parameters) :
  AuxKernel(parameters),
   _disp_x_var(coupled("disp_x")),
   _disp_y_var(coupled("disp_y")),
   _disp_z_var(coupled("disp_z")),
   _antiferrodis_A_x_var(coupled("antiferrodis_A_x")),
   _antiferrodis_A_y_var(coupled("antiferrodis_A_y")),
   _antiferrodis_A_z_var(coupled("antiferrodis_A_z")),
   _disp_x_grad(coupledGradient("disp_x")),
   _disp_y_grad(coupledGradient("disp_y")),
   _disp_z_grad(coupledGradient("disp_z")),
   _antiferrodis_A_x(coupledValue("antiferrodis_A_x")),
   _antiferrodis_A_y(coupledValue("antiferrodis_A_y")),
   _antiferrodis_A_z(coupledValue("antiferrodis_A_z")),
   _r11(getParam<Real>("r11")),
   _r12(getParam<Real>("r12")),
   _r44(getParam<Real>("r44")),
  _len_scale(getParam<Real>("len_scale"))
{
}

Real
RotostrictiveCouplingEnergyDensity::computeValue()
{
  return -(-2.0*_r44*((_antiferrodis_A_x[_qp]*_antiferrodis_A_y[_qp]*(_disp_x_grad[_qp](1) + _disp_y_grad[_qp](0)))/2.0 + (_antiferrodis_A_x[_qp]*_antiferrodis_A_z[_qp]*(_disp_x_grad[_qp](2) + _disp_z_grad[_qp](0)))/2.0 + (_antiferrodis_A_y[_qp]*_antiferrodis_A_z[_qp]*(_disp_y_grad[_qp](2) + _disp_z_grad[_qp](1)))/2.0) - _r12*((std::pow(_antiferrodis_A_y[_qp],2) + std::pow(_antiferrodis_A_z[_qp],2))*_disp_x_grad[_qp](0) + (std::pow(_antiferrodis_A_x[_qp],2) + std::pow(_antiferrodis_A_z[_qp],2))*_disp_y_grad[_qp](1) + (std::pow(_antiferrodis_A_x[_qp],2) + std::pow(_antiferrodis_A_y[_qp],2))*_disp_z_grad[_qp](2)) - 
   _r11*(std::pow(_antiferrodis_A_x[_qp],2)*_disp_x_grad[_qp](0) + std::pow(_antiferrodis_A_y[_qp],2)*_disp_y_grad[_qp](1) + std::pow(_antiferrodis_A_z[_qp],2)*_disp_z_grad[_qp](2))) * std::pow(_len_scale,3);
}
